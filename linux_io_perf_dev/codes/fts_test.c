#include <sys/types.h>
#include <sys/stat.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fts.h>

int main(int argc, char* argv[])
{
    char* paths[2] = {0, 0};
    paths[0] = argc == 2 ? strdup(argv[1]) : strdup(".");

    FTS* fts = fts_open(paths, 0, 0);
    if (fts == 0) {
        free(paths[0]);
        perror("fts_open");
        return 1;
    }
  
    uint64_t total_bytes = 0; 
    while(1) {
        FTSENT* ent = fts_read(fts);
        if (ent == 0) {
            printf("walk end\n");
            break;
        }
        unsigned short flag = ent->fts_info;
        if (flag == FTS_F)
            total_bytes += ent->fts_statp->st_size;
        printf("%-3s %2d %7jd   %-40s %ld %s %ld K\n",
            (flag == FTS_D) ?   "d"   : (flag == FTS_DNR) ? "dnr" :
            (flag == FTS_DP) ?  "dp"  : (flag == FTS_F) ?   "f" :
            (flag == FTS_NS) ?  "ns"  : (flag == FTS_SL) ?  "sl" :
            (flag == FTS_SLNONE) ? "sln" : "???",
            ent->fts_level, ent->fts_statp->st_size, 
            ent->fts_path, ent->fts_number, ent->fts_name, total_bytes >> 10);
    }

    free(paths[0]);
    fts_close(fts);
}
