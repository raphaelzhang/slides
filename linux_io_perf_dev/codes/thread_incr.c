#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

static volatile int count = 0;   /* "volatile" prevents compiler optimizations
                                   of arithmetic operations on 'count' */
static void *threadFunc(void *arg)
{
    int loops = *((int *) arg);
    for (int j = 0; j < loops; j++)
        count++;
    return NULL;
}

int main(int argc, char *argv[])
{
    int loops = (argc > 1) ? atoi(argv[1]) : 10000000;

    pthread_t t1, t2;
    int s = pthread_create(&t1, NULL, threadFunc, &loops);
    if (s != 0)
        exit(1);
    s = pthread_create(&t2, NULL, threadFunc, &loops);
    if (s != 0)
        exit(2);

    s = pthread_join(t1, NULL);
    if (s != 0)
        exit(3);
    s = pthread_join(t2, NULL);
    if (s != 0)
        exit(4);

    printf("count = %d\n", count);
}
