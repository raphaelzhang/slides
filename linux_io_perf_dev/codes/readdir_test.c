#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>

void listdir(const char *name, int level)
{
    DIR *dir;
    struct dirent *entry;

    if (!(dir = opendir(name)))
        return;
    if (!(entry = readdir(dir)))
        return;

    do {
        char path[1024];
        int len = snprintf(path, sizeof(path)-1, "%s/%s", name, entry->d_name);
        path[len] = 0;

        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            printf("%*s[%s]\n", level*2, "", entry->d_name);
            listdir(path, level + 1);
        }
        else {
            struct stat info = {0};
            if (lstat(path, &info) != 0)
                perror("lstat");
            printf("%*s- %s %ld\n", level*2, "", entry->d_name, info.st_size);
        }
    } while (entry = readdir(dir));

    closedir(dir);
}

int main(int argc, char* argv[])
{
    listdir(argc == 2 ? argv[1] : ".", 0);
    return 0;
}
