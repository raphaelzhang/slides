set term svg size 1280,720 enhanced font "Consolas,10"
set output "cpu.svg"
set key left top
set key box
set xdata time
set timefmt "%H:%M:%S"
set format x "%M:%S"
set autoscale
set grid
set tics scale 0
set style line 1 lc rgb "#ff0000" lt 1 pt 7 pi -1 ps 1
set style line 2 lc rgb "#00ff00" lt 1 pt 7 pi -1 ps 1
set style line 3 lc rgb "#0000ff" lt 1 pt 7 pi -1 ps 1
set pointintervalbox 2
set ylabel "% CPU"
plot 'sar.cpu.log' using 1:2 w linespoints ls 1 t "user", '' using 1:3 w linespoints ls 2 t "sys", '' using 1:4 w linespoints ls 3 t "iowait"
