#!/bin/bash
            
sar -f $1 -u | \
awk '{
if($2 == "all" && $1 ~ /^[0-9]/) 
{
    gsub("(时|分)", ":");
    gsub("秒", "");
    print $1, $3, $5, $6, $8
}
}' > sar.cpu.log

gnuplot sar.cpu.plt
rm sar.cpu.log